"use strict";
// ==UserScript==
// @name            tmmanager
// @description     qwe
// @namespace       Habrahabr
// @version         1.1
// @updateURL       https://gitlab.com/romanbereznov/tmmanager/-/raw/master/userscript.js

// @include         https://habr.com/ru/docs/help/rules/

// @connect habr.com

// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js
// require      https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.12/vue.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.12/vue.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment-with-locales.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/ramda/0.25.0/ramda.min.js

// @grant GM_addStyle
// @grant GM_xmlhttpRequest
// @grant GM_setValue
// @grant GM_getValue
// ==/UserScript==

const host = 'https://habr.com'
const basePath = 'ru';

const store = {
  get(key) {
    return Promise.resolve(GM_getValue(key)).then(dataJson => {
      return dataJson ? JSON.parse(dataJson) : null;
    });
  },
  set(key, data) {
    return this.get(key).then(() => {
      GM_setValue(key, JSON.stringify(data));
    });
  }
}

const corsThroughHttpRequest = (arg) => new Promise((resolve, reject) => {
  GM_xmlhttpRequest({
    ...arg,
    onload({ response }) {
      resolve(response);
    }
  });
});

const harbParsePosts = (html) => {
  const virtualDom = document.implementation.createHTMLDocument('virtualDom');
  const jqdoc = $(html, virtualDom)
  const href = $(".arrows-pagination__item-link_next", jqdoc).attr('href')
  const newUrl = href ? new URL(href, host) : null
  const posts = [];
  const $posts = $(".content-list > .content-list__item_post[id*='post']", jqdoc);
  $posts.each((i, post) => {
    let $title = $(post).find(".post__title_link")
    if (!$title.length) $title = $(post).find(".preview-data__title-link")
    let reading_count = Number($(post).find(".post-stats__views-count").text().replace(",", ".").replace("k", "e3"));
    let hubs = Array.from($(post).find(".post__hubs a, .preview-data__hubs a").map(function () {
      const [, hubSlub] = /\/hub*\/(.+)\//gim.exec($(this).attr('href')) || [null, null];
      return hubSlub;
    })).filter(Boolean);
    posts.push({
      date: parseDate($(post).find(".post__time").text() || $(post).find(".preview-data__time-published").text()),
      url: $title.attr("href"),
      title: $title.text(),
      reading_count,
      hubs,
    })
  });
  if (posts.find(post => !post.url)) debugger
  return {
    nextUrl: newUrl,
    posts,
  }
};

const habrFetchPosts = (hub, perDates, page) => {
  const hubPrefix = hub ? "hub" : "";
  const pagePath = `page${page}`
  const url = new URL([basePath, hubPrefix, hub, perDates, pagePath].filter(Boolean).join('/'), host);
  return corsThroughHttpRequest({
    method: 'GET',
    url,
  });
}

const parseDate = (dateString) => {
  if (dateString.includes('сегодня')) {
    return 0
  }
  if (dateString.includes('вчера')) {
    return 1
  }
  moment.locale('ru');
  return -moment(dateString, 'DD MMMM YYYY в hh:mm').diff(moment(), 'days');
}

const createHabrProvider = async function* (hub, perDates) {
  let page = 1
  let isNextPageExist = true;
  while (isNextPageExist) {
    let { posts, nextUrl } = await habrFetchPosts(hub, perDates, page).then(harbParsePosts);
    page += 1;
    isNextPageExist = Boolean(nextUrl);
    yield posts;
  }
}

const settingsComponent = Vue.component('settings', {
  data() {
    return {
      isShow: false,
    }
  },
  props: {
    source: Object,
    views: Object,
  },
  created() {
    //
  },
  computed: {
    hubs() {
      return Object.keys(this.source).sort((a, b) => this.views[b].like - this.views[a].like);
    },
    perDatesOptions() {
      return Object.keys(this.source['']);
    }
  },
  methods: {
    handlerChange() {
      this.$emit("change", this.source)
    }
  },
  template: `
  <div>
    <label>
      <input type='checkbox' v-model='isShow'>Show settings
    </label>
    <table v-if='isShow'>
      <thead>
      <tr>
        <td></td>
        <td></td>
        <td v-for="perDatesOption in perDatesOptions" :key="perDatesOption">{{perDatesOption}}</td>
      </tr>
      </thead>
      <tbody>
      <tr v-for="hub in hubs" :key="hub">
        <td>{{hub || "all"}}</td>
        <td>{{views[hub].like}}/{{views[hub].count}}</td>
        <td v-for="(perDatesOption,index) in perDatesOptions" :key="perDatesOption">
          <div class='settings__cell' >
          <input
            type='checkbox'
            v-model='source[hub][perDatesOption].checked'
            @change='handlerChange()'
          >
          <input
            class='settings__max-page'
            type='number'
            min=1
            v-model='source[hub][perDatesOption].targetPage'
            @change='handlerChange()'
            >
          </div>
        </td>
      </tr>
      </tbody>
    </table>
  </div>
  `
});
const postsComponent = Vue.component('posts', {
  props: {
    posts: Array,
  },
  data() {
    return {}
  },
  template: `
  <table>
    <thead>
      <tr>
        <td>hubs</td>
        <td>perDate</td>
        <td>page</td>
        <td>day passed</td>
        <td>title</td>
        <td>viewed</td>
        <td></td>
      </tr>
    </thead>
    <tbody name="list" is="transition-group">
          <tr v-for='(post,index) in posts' :key='post.url'>
            <td>
              <div class='post__label-container'>
              <span v-for='(hub) in post.hubs' :key='hub' class='post__label-hub'>
              {{hub || "all"}}
              </span>
              </div>
            </td>
            <td>
              <div class='post__label-container'>
              <span v-for='(perDatesOption) in post.perDatesOptions' :key='perDatesOption' class='post__label-perDatesOption'>
              {{perDatesOption}}
              </span>
              </div>
            </td>
            <td>{{[...post.pages].join(', ')}}</td>
            <td>{{post.date}}</td>
            <td><a :href='post.url' target=_blank class=tm-post__title-link>{{post.title}}</a></td>
            <td>{{post.reading_count}}</td>
            <td>
            <div class=post__controll-container>  
            <button class="btn"  @click='$emit("removePost",{...post,like:true})'>👍</button>
            <button class="btn"  @click='$emit("removePost",post)'>&#10006;</button>
            </div>
            </td>
          </tr>
    </tbody>
  </table>
  `
})

const Setting = class {
  constructor(store) {
    this.store = store;
  }
  get hubs() {

  }
  get perDatesOptions() {
    return [
      'top/alltime',
      'top/yearly',
      'top/monthly',
      'top/weekly',
      'top/daily',
    ]
  }
  async load() {
    this.state = await this.store.get('settings') || {}
  }
  async save() {
    await store.set('settings', this.state);
  }
  set(newState) {
    this.state = newState;
    this.save();
  }
  get() {
    return this.state;
  }
  addHubs(hubSlugs) {
    hubSlugs.forEach(hubSlug => {
      if (!this.state[hubSlug]) {
        this.state[hubSlug] = {}
        this.perDatesOptions.forEach(perDatesOption => {
          this.state[hubSlug][perDatesOption] = {
            perDatesOption, hub: hubSlug, targetPage: 1, checked: false
          }
        })
      }
    })
    return this.save();
  }
}

async function init() {
  new Vue({
    components: {
      posts: postsComponent,
      settings: settingsComponent,
    },
    data() {
      return {
        removedPosts: [],
        rawPosts: [],
        viewedUrls: {},
        isLoading: false,
        settingsReative: {},
        views: {},
      }
    },
    loadsState: [],
    methods: {
      appendPosts(posts, setting, page) {
        const { perDatesOption } = setting;
        const postsMap = this.rawPosts.reduce((acc, post) => {
          acc[post.url] = post;
          return acc
        }, {});
        posts.forEach(post => {
          if (postsMap[post.url]) {
            postsMap[post.url].perDatesOptions.add(perDatesOption)
            postsMap[post.url].pages.add(page)
          } else {
            postsMap[post.url] = {
              ...post,
              perDatesOptions: new Set([perDatesOption]),
              pages: new Set([page])
            }
          }
        });
        this.rawPosts = Object.values(postsMap);
      },
      getLoadStat(setting) {
        const { hub, perDatesOption } = setting;
        const loadStat = R.find(R.both(R.propEq('hub', hub), R.propEq('perDatesOption', perDatesOption)))(this.$options.loadsState);
        if (!loadStat) {
          const newLoadStat = {
            provider: createHabrProvider(hub, perDatesOption),
            currentPage: 0,
            targetPage: setting.targetPage,
          }
          this.$options.loadsState.push(newLoadStat)
          return newLoadStat
        } else {
          loadStat.setting = setting.targetPage;
          return loadStat
        }
      },
      async loadNext() {
        this.isLoading = true;
        const settingsEnabled = R.pipe(
          R.toPairs,
          R.map(R.nth(1)),
          R.map(R.toPairs),
          R.map(R.map(R.nth(1))),
          R.flatten,
          R.filter(R.propEq('checked', true))
        )(this.settings.get())
        for (const setting of settingsEnabled) {
          const loadStat = this.getLoadStat(setting);
          for await (let posts of loadStat.provider) {
            loadStat.currentPage++;
            this.appendPosts(posts, setting, loadStat.currentPage)
            const newHubs = posts.map(({ hubs }) => hubs).flat()
            this.settings.addHubs(newHubs);
            newHubs.forEach(hubSlug => {
              if (!this.views[hubSlug]) {
                this.views[hubSlug] = { count: 0, like: 0 }
              }
            })
            if (loadStat.currentPage >= loadStat.targetPage) {
              break;
            }
          }
        }
        store.set('views', this.views);
        this.settingsReative = this.settings.get();
        this.isLoading = false;
      },
      handlerRemovePost(post, index) {
        this.$set(this.viewedUrls, post.url, true);
        this.removedPosts.push(post);
        store.set('viewedUrls', this.viewedUrls);
        post.hubs.forEach(hubSlug => {
          this.views[hubSlug].count += 1
          if (post.like) {
            this.views[hubSlug].like += 1
          }
        })
        store.set('views', this.views);
      },
    },
    computed: {
      posts() {
        const posts = R.pipe(
          R.uniqBy(R.prop('url')),
          R.filter(post => !this.viewedUrls[post.url]),
          R.sortBy(R.pipe(R.prop('reading_count'), R.negate)),
        )(this.rawPosts);
        return posts
      }
    },
    async created() {
      this.viewedUrls = await store.get('viewedUrls') || {};
      this.views = await store.get('views') || { "": { count: 0, like: 0 } };
      this.settings = new Setting(store);
      await this.settings.load()
      await this.settings.addHubs([''])
      this.loadNext()
    },
    el: '.h-info_rules',
    template:
      `
    <div>
      <settings @change="settings.set($event);loadNext()" :source="settingsReative" :views='views'/>
      <div>Всего постов скачано: {{rawPosts.length}} | показано: {{posts.length}} | скрыто:{{rawPosts.length-posts.length}} | скрыто всего:{{Object.keys(viewedUrls).length}} </div>
      <posts :posts="posts" @removePost='handlerRemovePost($event)'/>
      <h3>Удаленные</h3>
      <posts :posts="removedPosts"/>
    </div>
`,
  });
  GM_addStyle(`
  .btn{
      cursor:pointer;
      transition: all 0.3s;
      box-shadow: 0 0 0px 0px red;
      padding:3px;
      border:1px solid green;
   }
  .btn:hover{
     box-shadow: 0 0 3px 3px red;
   }
  .list-  move {
    transition: transform 0.3s;
  }
  .list-enter-active{
    transition: all 0.3s;
  }
  .list-leave-active {
    transition: all 0.01s;
  }
  .list-enter, .list-leave-to {
    opacity: 0;
    transform: translateY(45px);
  }
  .post__label-perDatesOption {
    background: rgba(0,200,0,0.5);
    display: inline-block;
    padding: 1px;
    border-radius: 5px;
    margin:1px;
    font-size:10px;
    line-height:1.5;
  }
  .post__label-hub {
    background: rgba(200,0,0,0.5);
    display: inline-block;
    padding: 1px;
    border-radius: 5px;
    margin:1px;
    font-size:10px;
    line-height:1.5;
  }
  .settings__max-page {
    width:44px;
  }
  .settings__cell {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .post__label-container{
    display:flex;
    flex-wrap:wrap;
  }
  .post__controll-container{
    display:flex;  
  }
`)
}

! function (win) {
  if (window != window.top) return
  win.addEventListener("load", setTimeout.bind(null, init, 999), false);
}(typeof unsafeWindow == 'undefined' ? window : unsafeWindow);